# Text Manipulation
During the creation of this project I needed to find a linux group's GID by the group's name. I came up with a nitfy way to quickly do so with `awk`.

`awk -F":" '{print "GroupName: "$1 ", GID: "$3}' /etc/group`

I could scan the resulting manually or `|grep $GROUPNAME` to quickly find the GID of the group I was looking for. 

If I knew what I was looking for from the start this would work too `awk -F":" '/daemon/{print "GroupName: "$1 ", GID: "$3}' /etc/group` 

If I were to do this using Python it would look something like this:
```
import os, grp

def list_grps():
    for grp_id in os.getgroups():
        print(f"GroupName: {grp.getgrgid(grp_id).gr_name}, GID: {grp_id}")

def get_grp(name):
    g = grp.getgrnam(name)
    print(f"GroupName: {name}, GID: {g.gr_gid}")

# List all group names + GIDs
list_grps()

# Find a group named root
get_grp("daemon")
```

awk -F":" '/daemon/{print "GroupName: "$1 ", GID: "$3}' /etc/group