FROM ubuntu:18.04

WORKDIR /litecoin

# Install required packages as root
RUN apt update && apt install -y curl 

# Add a new system user ltc (src: mash up of https://stackoverflow.com/questions/27701930/add-user-to-docker-container)
RUN addgroup --system docker-litecoin && \
    useradd -rm -d /litecoin -s /bin/bash -g docker-litecoin -G sudo ltc && \
    chown -R ltc:docker-litecoin /litecoin
USER ltc

# Downloads resources and validate file for litecoin-0.17.1-x86_64-linux-gnu.tar.gz (TODO: turn into bash script i.e. validate.sh)
RUN mkdir -p /litecoin/data && \
    curl -o litecoin-0.17.1-x86_64-linux-gnu.tar.gz https://download.litecoin.org/litecoin-0.17.1/linux/litecoin-0.17.1-x86_64-linux-gnu.tar.gz && \
    LITECOIN_CHECKSUM=$(curl https://download.litecoin.org/litecoin-0.17.1/linux/litecoin-0.17.1-linux-signatures.asc | \
    grep "litecoin-0.17.1-x86_64-linux-gnu.tar.gz" | \
    awk -F  " " '{print $1}') && \
    DOWNLOAD_CHECKSUM=$(sha256sum litecoin-0.17.1-x86_64-linux-gnu.tar.gz | \
    awk -F  " " '{print $1}') && \
    [ "$DOWNLOAD_CHECKSUM" = "$LITECOIN_CHECKSUM" ] || (echo "[ERROR] CHECKSUMS DO NOT MATCH\n[ERROR] BUILD TERMINATING..." && exit 1) && \
    tar --remove-files --strip-components=1 -xvzf litecoin-0.17.1-x86_64-linux-gnu.tar.gz

# Drop in a custom conf
COPY litecoin.conf.sample /litecoin/litecoin.conf

CMD /litecoin/bin/litecoind -datadir=/litecoin/data -conf=/litecoin/litecoin.conf
