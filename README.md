# Litecoin StatefulSet
This repo contains a Dockerfile for launching litecoin core 0.17.1, a GitLab CI/CD pipeline, and a Kubernetes StatefulSet for running Litecoin core 0.17.1 in cluster.

## External Deps
Litecoin 0.17.1 Binaries: https://download.litecoin.org/litecoin-0.17.1/linux/litecoin-0.17.1-x86_64-linux-gnu.tar.gz

Litecoin 0.17.1 Checksum: https://download.litecoin.org/litecoin-0.17.1/linux/litecoin-0.17.1-linux-signatures.asc

## Ports
Defaults are used across the board. Default ports are listed below.

```
 -port=<port>
      Listen for connections on <port> (default: 9333 or testnet: 19335)
 -rpcport=<port>
      Listen for JSON-RPC connections on <port> (default: 9332 or testnet:
      19332)
```
** If you wish to take advantage of the rpc server then additional configuration in the `litecoin.conf` is required.

## Local Setup
This project assumes you have the latest version of Docker, kubectl, and Minikube installed and running on your system.

0. Docker Desktop Install for Mac (https://docs.docker.com/docker-for-mac/install/) && Install kubectl https://kubernetes.io/docs/tasks/tools/install-kubectl/#install-kubectl-on-macos && Minikube: `brew install minikube`

1. `minikube start --memory=8196 --cpus=4` (I start mine local cluster a little fat as I found litecoind to be a bit of a memory hog)
2. `k apply -f ltc.yml`
3. `k get pods -nltc-exercise -owide --watch` to see the pod come up and `k logs -nltc-exercise ltc-0` to view the logs
4. `kubectl port-forward -n ltc-exercise statefulset/ltc 9333:9333 9332:9332 19335:19335 19332:19332` to bind to localhost for testing, etc

Notes for local image deployment:
* `eval $(minikube docker-env)` and then `docker build -t ....` to ensure the image ends up in the minikube docker registry where the cluster can find it
* If deploying from the local minikube docker registry set the `imagePullPolicy` to `Never` to ensure the local registry image is used
* `eval $(minikube docker-env --unset)` to return to your local docker env

## Deployment
The `.gitlab-ci.yml` is configured to run 4 stages (build, test, push-latest, and deploy) for this project.

#### Stages
1. `build` - Builds a Docker Image tagging it with the branch slug
2. `test` - Runs the `litcoin_test` command to ensure everything is in working order on the freshly baked image
3. `push-latest` - Ships a latest tagged image when the branch is master (i.e. on a PR merge)
4. `deploy` - A psuedo deploy step as the StatefulSet is currently only configured for local dev (ideally it would deploy to a connected cluster)



